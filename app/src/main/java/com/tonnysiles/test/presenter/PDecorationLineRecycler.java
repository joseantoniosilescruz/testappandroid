package com.tonnysiles.test.presenter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tonnysiles.test.R;

public class PDecorationLineRecycler extends RecyclerView.ItemDecoration {
    private Drawable lineDividing;

    public PDecorationLineRecycler(Context context) {
        lineDividing = ContextCompat.getDrawable(context, R.drawable.drawable_line_recycler);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int space = 40;
        int left = parent.getPaddingLeft() + space;
        int right = parent.getWidth() - parent.getPaddingRight() - space;

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + lineDividing.getIntrinsicHeight();

            lineDividing.setBounds(left, top, right, bottom);
            lineDividing.draw(c);
        }
    }
}

