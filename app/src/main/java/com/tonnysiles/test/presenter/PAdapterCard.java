package com.tonnysiles.test.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tonnysiles.test.R;
import com.tonnysiles.test.model.service.CardResponse;

import java.util.List;

public class PAdapterCard  extends RecyclerView.Adapter<PAdapterCard.ViewHolder> implements View.OnClickListener{

    private View.OnClickListener listener;
    private List<CardResponse> cards;
    private Context context;

    public PAdapterCard(List<CardResponse> cards, Context context) {
        this.cards = cards;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_card, viewGroup, false);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        CardResponse card = cards.get(i);
        viewHolder.iteTexNumber.setText(card.getNumber());
        switch (card.getType()){
            case "VISA": viewHolder.iteImaCard.setImageResource(R.drawable.ic_visa);
                break;
            case "MAST": viewHolder.iteImaCard.setImageResource(R.drawable.ic_mastercard);
                break;
            case "DINE": viewHolder.iteImaCard.setImageResource(R.drawable.ic_money);
                break;
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }
    /**********************************************************************************************/
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView iteImaCard;
        public TextView iteTexNumber;

        public ViewHolder(View v) {
            super(v);
            iteImaCard = (ImageView) v.findViewById(R.id.iteImaCard);
            iteTexNumber = (TextView) v.findViewById(R.id.iteTexNumber);
        }
    }
}