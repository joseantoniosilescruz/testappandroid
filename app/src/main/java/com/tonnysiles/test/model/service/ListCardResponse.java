package com.tonnysiles.test.model.service;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListCardResponse {
    @SerializedName("permite_efectivo")
    private String allowsCash;
    @SerializedName("listadoTarjetas")
    private ArrayList<CardResponse> cards;

    public ListCardResponse(String allowsCash, ArrayList<CardResponse> cards) {
        this.allowsCash = allowsCash;
        this.cards = cards;
    }

    public String getAllowsCash() {
        return allowsCash;
    }

    public void setAllowsCash(String allowsCash) {
        this.allowsCash = allowsCash;
    }

    public ArrayList<CardResponse> getCards() {
        return cards;
    }

    public void setCards(ArrayList<CardResponse> cards) {
        this.cards = cards;
    }
}
