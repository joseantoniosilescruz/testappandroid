package com.tonnysiles.test.model.service;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("ws.php")
    Call<ListCardResponse> getPlaylists();
}
