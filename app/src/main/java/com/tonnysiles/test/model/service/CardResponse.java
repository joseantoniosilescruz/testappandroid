package com.tonnysiles.test.model.service;

import com.google.gson.annotations.SerializedName;

public class CardResponse {
    @SerializedName("numero")
    private String number;
    @SerializedName("tipo")
    private String type;

    public CardResponse(String number, String type) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
