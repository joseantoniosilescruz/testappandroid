package com.tonnysiles.test.view;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tonnysiles.test.R;
import com.tonnysiles.test.model.service.ApiService;
import com.tonnysiles.test.model.service.CardResponse;
import com.tonnysiles.test.model.service.ListCardResponse;
import com.tonnysiles.test.model.service.RetrofitInstance;
import com.tonnysiles.test.presenter.PAdapterCard;
import com.tonnysiles.test.presenter.PDecorationLineRecycler;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CardsFragment extends Fragment {

    private RecyclerView carRevListCards;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cards, container, false);
        carRevListCards = (RecyclerView)root.findViewById(R.id.carRevListCards);
        LinearLayoutManager linearLayout = new LinearLayoutManager(getActivity());
        carRevListCards.setLayoutManager(linearLayout);
        initToolbar(root);
        new MyThread().start();
        return root;
    }
    private void initToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.tooCards);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.methods));
        toolbar.inflateMenu(R.menu.menu_home);

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0){
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
    }
    class MyThread extends Thread {
        @Override
        public void run() {
            try {
                Retrofit retrofit = RetrofitInstance.getRetrofitInstance();
                ApiService service = retrofit.create(ApiService.class);

                Call<ListCardResponse> call = service.getPlaylists();

                call.enqueue(new Callback<ListCardResponse>() {
                    @Override
                    public void onResponse(Call<ListCardResponse> call, Response<ListCardResponse> response) {
                        final ArrayList<CardResponse> items = response.body().getCards();
                        final PAdapterCard adapter = new PAdapterCard(items, getContext());
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                carRevListCards.setAdapter(adapter);
                                carRevListCards.addItemDecoration(new PDecorationLineRecycler(getContext()));
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<ListCardResponse> call, Throwable t) {
                        Log.d("TAG_APP", "Something went wrong...Error message: " + t.getMessage());
                    }
                });
            } catch (Exception e) {
                Log.d("TAG_APP", e.getMessage());
            }
        }
    }
}
