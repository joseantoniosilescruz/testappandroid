package com.tonnysiles.test.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tonnysiles.test.R;

public class HomeFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        Button metButConfirm = (Button)root.findViewById(R.id.metButConfirm);
        metButConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardsFragment nextFrag= new CardsFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fraContainer, nextFrag, "CardsFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });
        initToolbar(root);
        return root;
    }
    private void initToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.tooHome);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.methods));
        toolbar.inflateMenu(R.menu.menu_home);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
    }
}